from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem


# Create your views here.
def todo_list_list(request):
    todolist = TodoList.objects.all()
    context = {"todo_list_list": todolist}

    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    list = TodoList.objects.get(id=id)
    context = {
        "list": list,
    }
    return render(request, "todos/detail.html", context)
